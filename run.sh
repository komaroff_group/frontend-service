#!/bin/bash

source ./common.sh

docker run -p 3000:3000 ${DOCKER_IMAGE_BASENAME} "$@"
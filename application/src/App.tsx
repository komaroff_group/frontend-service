import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Route, Routes } from 'react-router';
import HomePage from './components/HomePage/HomePage';
import AboutProjectPage from './components/AboutProjectPage/AboutProjectPage';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import SearchResultsPage from './components/SearchResultsPage/SearchResultsPage';
import UserProfilePage from './components/UserProfilePage/UserProfilePage';
import UserOrdersPage from './components/UserOrdersPage/UserOrdersPage';
import UserFavoritesPage from './components/UserFavoritesPage/UserFavoritesPage';
import UserCartPage from './components/UserCartPage/UserCartPage';
import UserListsPage from './components/UserListsPage/UserListsPage';

function App() {
  return (
    <React.Fragment>
      <BrowserRouter>
        <Header />
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/about" element={<AboutProjectPage />} />
          <Route path="/search" element={<SearchResultsPage />} />

          <Route path="/user/profile" element={<UserProfilePage />} />
          <Route path="/user/orders" element={<UserOrdersPage />} />
          <Route path="/user/favorites" element={<UserFavoritesPage />} />
          <Route path="/user/waitings" element={<UserListsPage />} />
          <Route path="/user/cart" element={<UserCartPage />} />
        </Routes>
        <Footer />
      </BrowserRouter>
    </React.Fragment>
  );
}

export default App;

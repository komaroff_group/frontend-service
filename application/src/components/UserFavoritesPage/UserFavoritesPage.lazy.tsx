import React, { lazy, Suspense } from 'react';

const LazyUserFavoritesPage = lazy(() => import('./UserFavoritesPage'));

const UserFavoritesPage = (props: JSX.IntrinsicAttributes & { children?: React.ReactNode; }) => (
  <Suspense fallback={null}>
    <LazyUserFavoritesPage {...props} />
  </Suspense>
);

export default UserFavoritesPage;

import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import UserFavoritesPage from './UserFavoritesPage';

describe('<UserFavoritesPage />', () => {
  test('it should mount', () => {
    render(<UserFavoritesPage />);
    
    const userFavoritesPage = screen.getByTestId('UserFavoritesPage');

    expect(userFavoritesPage).toBeInTheDocument();
  });
});
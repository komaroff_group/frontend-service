import React, { FC } from 'react';
import styles from './UserFavoritesPage.module.scss';
import Main from '../Main/Main';

interface UserFavoritesPageProps { }

const UserFavoritesPage: FC<UserFavoritesPageProps> = () => (
  <div className={styles.root}>
    <Main>
      <div>
        <h1>
          Избранное
        </h1>
        <div>
          Здесь будут товары, которые Вам понравились.
        </div>
      </div>
    </Main>
  </div>
);

export default UserFavoritesPage;

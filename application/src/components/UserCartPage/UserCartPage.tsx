import React, { FC } from 'react';
import styles from './UserCartPage.module.scss';
import Main from '../Main/Main';

interface UserCartPageProps { }

const UserCartPage: FC<UserCartPageProps> = () => (
  <div className={styles.root}>
    <Main>
      <div>
        <h1>
          Корзина
        </h1>
        <div>
          Здесь будут Ваши товары, готовые к заказу.
        </div>
      </div>
    </Main>
  </div>
);

export default UserCartPage;

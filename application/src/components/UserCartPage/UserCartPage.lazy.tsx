import React, { lazy, Suspense } from 'react';

const LazyUserCartPage = lazy(() => import('./UserCartPage'));

const UserCartPage = (props: JSX.IntrinsicAttributes & { children?: React.ReactNode; }) => (
  <Suspense fallback={null}>
    <LazyUserCartPage {...props} />
  </Suspense>
);

export default UserCartPage;

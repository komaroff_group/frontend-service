import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import UserCartPage from './UserCartPage';

describe('<UserCartPage />', () => {
  test('it should mount', () => {
    render(<UserCartPage />);
    
    const userCartPage = screen.getByTestId('UserCartPage');

    expect(userCartPage).toBeInTheDocument();
  });
});
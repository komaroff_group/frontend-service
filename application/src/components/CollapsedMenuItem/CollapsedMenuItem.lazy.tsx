import React, { lazy, Suspense } from 'react';
import { CollapsedMenuItemProps } from './CollapsedMenuItem';

const LazyCollapsedMenuItem = lazy(() => import('./CollapsedMenuItem'));

const CollapsedMenuItem = (props: JSX.IntrinsicAttributes & { children?: React.ReactNode; } & CollapsedMenuItemProps) => (
  <Suspense fallback={null}>
    <LazyCollapsedMenuItem {...props} />
  </Suspense>
);

export default CollapsedMenuItem;

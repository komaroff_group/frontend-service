import React, { FC, MouseEventHandler } from 'react';
import { HierarchicalCategory } from '../CollapsedSubMenuItem/CollapsedSubMenuItem';
import styles from './CollapsedMenuItem.module.scss';

export interface RootCategory {
  id: any,
  name: string,
  categories: HierarchicalCategory[]
}
export interface CollapsedMenuItemProps {
  id: any,
  label: string,
  onMouseOver?: MouseEventHandler,
  onClick?: MouseEventHandler,
}

const CollapsedMenuItem: FC<CollapsedMenuItemProps> = (props) => (
  <div className={styles.collapsedMenuItem} data-testid="CollapsedMenuItem"
       onMouseOver={props.onMouseOver}
       onClick={props.onClick}
  >
    {props.label}
  </div>
);

export default CollapsedMenuItem;

import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import CollapsedMenuItem from './CollapsedMenuItem';

describe('<CollapsedMenuItem />', () => {
  test('it should mount', () => {
    render(<CollapsedMenuItem id={1} label={'Label 1'} />);
    
    const collapsedMenuItem = screen.getByTestId('CollapsedMenuItem');

    expect(collapsedMenuItem).toBeInTheDocument();
  });
});
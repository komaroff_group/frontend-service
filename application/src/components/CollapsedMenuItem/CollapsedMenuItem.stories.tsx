/* eslint-disable */
import CollapsedMenuItem from './CollapsedMenuItem';

export default {
  title: "CollapsedMenuItem",
};

export const Default = () => <CollapsedMenuItem id={0} label={""} />;

Default.story = {
  name: 'default',
};

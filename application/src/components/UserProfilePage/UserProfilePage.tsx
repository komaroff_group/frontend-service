import React, { FC } from 'react';
import styles from './UserProfilePage.module.scss';
import Main from '../Main/Main';

interface UserProfilePageProps { }

const UserProfilePage: FC<UserProfilePageProps> = () => (
  <div className={styles.root}>
    <Main>
      <div>
        <h1>
          Учетная запись
        </h1>
        <div>
          Здесь будут Ваши персональные данные, платежные карты,
          подписки и информация о программе лояльности.
        </div>
      </div>
    </Main>
  </div>
);

export default UserProfilePage;

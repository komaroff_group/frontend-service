import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import UserProfilePage from './UserProfilePage';

describe('<UserProfilePage />', () => {
  test('it should mount', () => {
    render(<UserProfilePage />);
    
    const userProfilePage = screen.getByTestId('UserProfilePage');

    expect(userProfilePage).toBeInTheDocument();
  });
});
import React, { lazy, Suspense } from 'react';

const LazyUserProfilePage = lazy(() => import('./UserProfilePage'));

const UserProfilePage = (props: JSX.IntrinsicAttributes & { children?: React.ReactNode; }) => (
  <Suspense fallback={null}>
    <LazyUserProfilePage {...props} />
  </Suspense>
);

export default UserProfilePage;

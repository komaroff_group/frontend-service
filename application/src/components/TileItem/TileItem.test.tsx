import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import TileItem from './TileItem';

describe('<TileItem />', () => {
  test('it should mount', () => {
    render(<TileItem />);
    
    const tileItem = screen.getByTestId('TileItem');

    expect(tileItem).toBeInTheDocument();
  });
});
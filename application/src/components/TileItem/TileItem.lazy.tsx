import React, { lazy, Suspense } from 'react';

const LazyTileItem = lazy(() => import('./TileItem'));

const TileItem = (props: JSX.IntrinsicAttributes & { children?: React.ReactNode; }) => (
  <Suspense fallback={null}>
    <LazyTileItem {...props} />
  </Suspense>
);

export default TileItem;

import React, { FC, SyntheticEvent } from 'react';
import { useNavigate } from 'react-router-dom';
import DeliveryInfoView from '../DeliveryInfoView/DeliveryInfoView';
import RatingView, { RatedObjectType } from '../RatingView/RatingView';
import styles from './TileItem.module.scss';
import moment from 'moment';

export interface TileItemData {
  id: any,
  title: string,
  imageUrl: string,
  price: number,
  forecastedDeliveryDate: number
}

interface TileItemProps {
  type?: string,
  data?: TileItemData
}

const TileItem: FC<TileItemProps> = (props) => {
  const imageUrl = props.data?.imageUrl;
  const isImagePresent = !!imageUrl;
  const imageBlock = isImagePresent ?
    (
      <div className={styles.itemImagePresent}
           style={{
            backgroundImage: `url('${imageUrl}')`
           }}
      ></div>
    ) :
    (
      <div className={styles.itemImageAbsent}></div>
    );

  const navigate = useNavigate();
  const onTileItemClick = () => {
    navigate(`/products?id=${props.data?.id}`);
  }

  const onAddToCartButtonClick = (e: SyntheticEvent) => {
    e.stopPropagation();
    alert("Added to cart.");
    console.log("Event: ", e);
    console.log("Item data: ", props.data);
  }

  /* TODO Remove It */
  const randomInt = (a: number, b: number) => {
    return Math.round(a + Math.random() * b);
  }

  /* TODO Remove It */
  const _tempGenerateRates = () => {
    let array = [];
    let ratesCount = randomInt(5, 100);
    for (let i = 0; i < ratesCount; i++) {
      array.push(randomInt(1, 5));
    }
    return array;
  }

  return (
    <div className={styles.tileItemContainer} data-testid="TileItem"
         onClick={onTileItemClick}
    >
      <div className={styles.itemImageContainer}>
        {imageBlock}
      </div>
      <RatingView for={{
        id: props.data?.id,
        type: RatedObjectType.PRODUCT,
        rates: _tempGenerateRates()
      }} />
      <span className={styles.itemTitle}>{props.data?.title}</span>
      <span className={styles.itemPrice}>{props.data?.price} &#8381;</span>
      <DeliveryInfoView forecastedDeliveryDate={moment.unix(props.data?.forecastedDeliveryDate as number)} />
      <button className={styles.smallButton}
              onClick={onAddToCartButtonClick}
      >В корзину</button>
    </div>
  );
};

export default TileItem;

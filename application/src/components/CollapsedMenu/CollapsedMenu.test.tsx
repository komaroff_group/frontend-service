import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import CollapsedMenu from './CollapsedMenu';

describe('<CollapsedMenu />', () => {
  test('it should mount', () => {
    render(<CollapsedMenu />);
    
    const collapsedMenu = screen.getByTestId('CollapsedMenu');

    expect(collapsedMenu).toBeInTheDocument();
  });
});
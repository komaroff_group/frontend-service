import React, { FC, useEffect, useRef, useState } from 'react';
import useOutsideAlerter from '../../hooks/useOutsideAlerter';
import CollapsedMenuItem, { RootCategory } from '../CollapsedMenuItem/CollapsedMenuItem';
import CollapsedSubMenuItem, { HierarchicalCategory } from '../CollapsedSubMenuItem/CollapsedSubMenuItem';
import styles from './CollapsedMenu.module.scss';

interface CollapsedMenuProps { }

const CollapsedMenu: FC<CollapsedMenuProps> = () => {

  function generateMenuItems(): RootCategory[] {
    const menuItems: RootCategory[] = [];
    for (let i = 0; i < 5; i++) {
      const menuItem: RootCategory = {
        id: i,
        name: `Root Category ${i}`,
        categories: []
      };

      let subSize = 0;
      if (i === 3) {
        subSize = 25;
      } else if (i === 4) {
        subSize = 10;
      }

      if (subSize > 0) {
        for (let j = 0; j < subSize; j++) {
          const hierarchicalCategory: HierarchicalCategory = {
            id: j,
            name: `Tree Category ${i}-${j}`,
            subCategories: []
          };

          for (let k = 0; k < 4; k++) {
            const category = {
              id: k,
              name: `Category ${j}-${k}`
            };
            hierarchicalCategory.subCategories.push(category);
          }

          menuItem.categories.push(hierarchicalCategory);
        }
      }

      menuItems.push(menuItem);
    }
    return menuItems;
  }

  function onHoverMainMenuItem(this: RootCategory, event: any) {
    setSubMenuItems(this.categories);
    setSubMenuVisible(this.categories.length > 0);
  }

  const [mainMenuVisible, setMainMenuVisible] = useState(false);
  const [subMenuVisible, setSubMenuVisible] = useState(false);
  const [menuItems, setMenuItems] = useState([] as RootCategory[]);
  const [subMenuItems, setSubMenuItems] = useState([] as HierarchicalCategory[]);

  const onOpenMenuButtonClick = () => {
    setMainMenuVisible(!mainMenuVisible);
  };

  const onClickOutside = () => {
    setSubMenuVisible(false);
    setMainMenuVisible(false);
  };

  const onMenuItemClick = (event: any) => {
    event.stopPropagation();
    alert("Aloha");
  };

  useEffect(() => {
    const menuItems = generateMenuItems();
    setMenuItems(menuItems);
  }, []);

  const collapsedMenuRef = useRef(null);
  useOutsideAlerter(collapsedMenuRef, onClickOutside);

  return (
    <div className={styles.collapsedMenuContainer} data-testid="CollapsedMenu"
      ref={collapsedMenuRef}
    >
      <button className={styles.categoriesListButton}
        title="Категории товаров"
        onClick={onOpenMenuButtonClick}
      ></button>
      <div className={styles.mainList}
        style={{
          display: mainMenuVisible ? '' : 'none'
        }}
      >
        {menuItems.map((menuItem: any) =>
          <CollapsedMenuItem
            key={menuItem.id}
            id={menuItem.id}
            label={menuItem.name}
            onMouseOver={onHoverMainMenuItem.bind(menuItem)}
            onClick={onMenuItemClick}
          />
        )}
      </div>
      <div className={styles.subList}
        style={{
          display: subMenuVisible ? 'flex' : 'none'
        }}
      >
        {subMenuItems.map((hierarchicalCategory: HierarchicalCategory) =>
          <CollapsedSubMenuItem
            key={hierarchicalCategory.id}
            categories={hierarchicalCategory}
          />
        )}
      </div>
    </div>
  );
};

export default CollapsedMenu;

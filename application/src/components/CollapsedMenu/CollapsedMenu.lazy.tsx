import React, { lazy, Suspense } from 'react';

const LazyCollapsedMenu = lazy(() => import('./CollapsedMenu'));

const CollapsedMenu = (props: JSX.IntrinsicAttributes & { children?: React.ReactNode; }) => (
  <Suspense fallback={null}>
    <LazyCollapsedMenu {...props} />
  </Suspense>
);

export default CollapsedMenu;

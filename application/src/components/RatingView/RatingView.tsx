import React, { FC } from 'react';
import styles from './RatingView.module.scss';

export enum RatedObjectType {
  PRODUCT, SHOP, BRAND
}

export interface RatedObject {
  id: any,
  type: RatedObjectType,
  rates: number[]
}

interface RatingViewProps {
  for: RatedObject
}

function getSizeInPixels(numberSize: number): string {
  return `${numberSize}px`;
}

function calculateRating(props: RatingViewProps): any {
  const sum = props.for.rates.reduce((totalRating, currentValue) => {
    return totalRating + currentValue;
  }, 0);
  const rating = sum / props.for.rates.length;
  return {
    stars: rating,
    rating: rating.toFixed(1)
  };
}

const RatingView: FC<RatingViewProps> = (props) => {

  const totalStarsCount = 5;
  const starsSizePx = 20;

  const {stars: actualStarsCount, rating} = calculateRating(props);

  const starsViewCalculatedStyles = {
    boxShadow: `inset ${getSizeInPixels(actualStarsCount * starsSizePx)} 0 red`,
    maskSize: getSizeInPixels(starsSizePx),
    WebkitMaskSize: getSizeInPixels(starsSizePx),
    width: getSizeInPixels(totalStarsCount * starsSizePx),
    height: getSizeInPixels(starsSizePx)
  };

  const numberViewCalculatedStyles = {
    fontSize: getSizeInPixels(0.75 * starsSizePx)
  };

  return (
    <div className={styles.ratingView}
         title={`Рейтинг на основе ${props.for.rates.length} оценок`}
    >
      <div className={styles.starsView} 
           style={starsViewCalculatedStyles}
           data-testid="RatingView">
      </div>
      <span className={styles.numberView}
            style={numberViewCalculatedStyles}
      >{rating}</span>
    </div>
  );
};

export default RatingView;

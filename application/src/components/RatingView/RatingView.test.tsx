import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import RatingView, { RatedObjectType } from './RatingView';

describe('<RatingView />', () => {
  test('it should mount', () => {
    render(<RatingView for={{
      id: '100500',
      type: RatedObjectType.PRODUCT,
      rates: [1, 3, 5]
    }} />);
    
    const ratingView = screen.getByTestId('RatingView');

    expect(ratingView).toBeInTheDocument();
  });
});
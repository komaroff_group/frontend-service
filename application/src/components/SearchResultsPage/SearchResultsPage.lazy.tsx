import React, { lazy, Suspense } from 'react';

const LazySearchResultsPage = lazy(() => import('./SearchResultsPage'));

const SearchResultsPage = (props: JSX.IntrinsicAttributes & { children?: React.ReactNode; }) => (
  <Suspense fallback={null}>
    <LazySearchResultsPage {...props} />
  </Suspense>
);

export default SearchResultsPage;

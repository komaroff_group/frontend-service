import React, { FC } from 'react';
import Main from '../Main/Main';
import styles from './SearchResultsPage.module.scss';

interface SearchResultsPageProps {}

const SearchResultsPage: FC<SearchResultsPageProps> = () => (
  <div className={styles.root}>
      <Main>
        <div>
          <h1>
            Результаты поиска
          </h1>
          <div>
            Кто ищет тот всегда найдет! Возможно, не здесь...
          </div>
        </div>
      </Main>
    </div>
);

export default SearchResultsPage;

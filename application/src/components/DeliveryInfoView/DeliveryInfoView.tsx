import { Moment } from 'moment';
import moment from 'moment';
import 'moment/locale/ru'
import React, { FC } from 'react';
import styles from './DeliveryInfoView.module.scss';

interface DeliveryInfoViewProps {
  forecastedDeliveryDate: Moment
}

function getPrettyForecastedDeliveryDate(date: Moment) {
  const isCurrentYear = moment().format("YYYY") === date.format("YYYY");
  let weekDay = date.format("dddd");
  let prefix = "в";
  switch (weekDay) {
    case "вторник":
      prefix = "во";
      break;
    case "среда":
    case "пятница":
    case "суббота":
      weekDay = `${weekDay.substring(0, weekDay.length - 1)}у`;
  }
  let suffix = !isCurrentYear ? 'YYYY года' : '';
  const dateString = `${weekDay}${date.format(`, DD MMMM ${suffix}`)}`.trim();
  return `${prefix} ${dateString}`;
}

const DeliveryInfoView: FC<DeliveryInfoViewProps> = (props) => {

  const now = moment().locale('ru');

  return (
    <div className={styles.deliveryInfoView} data-testid="DeliveryInfoView">
      <span>
        Доставим <b>{getPrettyForecastedDeliveryDate(props.forecastedDeliveryDate)}</b>
      </span>
    </div>
  );
};

export default DeliveryInfoView;

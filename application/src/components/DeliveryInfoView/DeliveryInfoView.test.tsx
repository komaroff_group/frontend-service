import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import DeliveryInfoView from './DeliveryInfoView';
import moment from 'moment';

describe('<DeliveryInfoView />', () => {
  test('it should mount', () => {
    render(<DeliveryInfoView forecastedDeliveryDate={moment()}/>);
    
    const deliveryInfoView = screen.getByTestId('DeliveryInfoView');

    expect(deliveryInfoView).toBeInTheDocument();
  });
});
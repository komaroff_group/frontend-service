import React, { FC, useEffect, useState } from 'react';
import { KeyCode } from '../../utils/CommonConstants';
import SearchHints from '../SearchHints/SearchHints';
import styles from './Search.module.scss';

interface SearchProps { }

const mockedSearchResults =  [
  {
    title: "Abakan"
  },
  {
    title: "Abakin"
  },
  {
    title: "Abakov"
  },
  {
    title: "Abanan"
  },
  {
    title: "Boris"
  },
  {
    title: "Mama-lama"
  },
  {
    title: "Blama"
  },
  {
    title: "Barabulka"
  },
  {
    title: "Yandex"
  },
  {
    title: "babu123shka"
  }
];

const Search: FC<SearchProps> = () => {
  const [searchQuery, setSearchQuery] = useState("");

  const [searchHintsVisible, setSearchHintsVisible] = useState(false);
  const [searchHints, setSearchHints] = useState(mockedSearchResults as any[]);

  useEffect(() => {
    let preparedSearchQuery = searchQuery.trim().toLocaleLowerCase();
    if (preparedSearchQuery.length >= 3) {
      setSearchHintsVisible(true);
      setSearchHints(mockedSearchResults
          .filter(hint => {
            let title = hint.title.trim().toLocaleLowerCase();
            return title.includes(searchQuery) ||
                  searchQuery.includes(title);
          }));
    } else {
      setSearchHintsVisible(false);
    }
  }, [searchQuery])

  const onInputChange = (e: any) => {
    setSearchQuery(e?.target?.value);
  }

  const onStartSearch = () => {
    if (searchQuery) {
      console.log(`User started search with query: "${searchQuery}"`);
    }
  }

  const onEnterPress = (e: any) => {
    if (e.keyCode === KeyCode.ENTER) {
      onStartSearch();
    }
  }

  return (
    <div className={styles.search} data-testid="Search">
      <input className={styles.searchInput}
        type="text"
        placeholder='Например, ёлочная гирлянда'
        onInput={onInputChange}
        onKeyDown={onEnterPress}
      />
      <button className={styles.searchButton}
              onClick={onStartSearch}
      ></button>
      <SearchHints visible={searchHintsVisible} 
                   items={searchHints} 
      />
    </div>
  );
};

export default Search;

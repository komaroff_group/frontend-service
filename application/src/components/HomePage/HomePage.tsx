import React from 'react';
import Main from '../Main/Main';
import styles from '../../App.module.scss';
import Tile from '../Tile/Tile';

function HomePage() {
  return (
    <div className={styles.root}>
      <Main>
        <h1>Главная страница</h1>
        <div>
          Добро пожаловать на <b>TheMarketplace</b>!
        </div>
        <Tile />
      </Main>
    </div>
  );
}

export default HomePage;

import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import HeaderLeftSide from './HeaderLeftSide';

describe('<HeaderLeftSide />', () => {
  test('it should mount', () => {
    render(<HeaderLeftSide />);
    
    const headerLeftSide = screen.getByTestId('HeaderLeftSide');

    expect(headerLeftSide).toBeInTheDocument();
  });
});
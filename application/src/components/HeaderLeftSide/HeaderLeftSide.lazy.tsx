import React, { lazy, Suspense } from 'react';

const LazyHeaderLeftSide = lazy(() => import('./HeaderLeftSide'));

const HeaderLeftSide = (props: JSX.IntrinsicAttributes & { children?: React.ReactNode; }) => (
  <Suspense fallback={null}>
    <LazyHeaderLeftSide {...props} />
  </Suspense>
);

export default HeaderLeftSide;

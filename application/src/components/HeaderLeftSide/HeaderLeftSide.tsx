import React, { FC } from 'react';
import CollapsedMenu from '../CollapsedMenu/CollapsedMenu';
import Logo from '../Logo/Logo';
import Search from '../Search/Search';
import styles from './HeaderLeftSide.module.scss';

interface HeaderLeftSideProps {}

const HeaderLeftSide: FC<HeaderLeftSideProps> = () => {

  return (
    <div className={styles.headerLeftSideContainer} data-testid="HeaderLeftSide">
      <Logo />
      <CollapsedMenu />
      <Search />
    </div>
  );
};

export default HeaderLeftSide;

import React, { FC } from 'react';
import styles from './UserListsPage.module.scss';
import Main from '../Main/Main';

interface UserListsPageProps { }

const UserListsPage: FC<UserListsPageProps> = () => (
  <div className={styles.root}>
    <Main>
      <div>
        <h1>
          Список ожидания
        </h1>
        <div>
          Здесь будет список товаров, которых нет в наличии, но Вы
          подписались на уведомления об их поступлении.
        </div>
      </div>
    </Main>
  </div>
);

export default UserListsPage;

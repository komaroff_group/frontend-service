import React, { lazy, Suspense } from 'react';

const LazyUserListsPage = lazy(() => import('./UserListsPage'));

const UserListsPage = (props: JSX.IntrinsicAttributes & { children?: React.ReactNode; }) => (
  <Suspense fallback={null}>
    <LazyUserListsPage {...props} />
  </Suspense>
);

export default UserListsPage;

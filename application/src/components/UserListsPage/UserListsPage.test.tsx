import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import UserListsPage from './UserListsPage';

describe('<UserListsPage />', () => {
  test('it should mount', () => {
    render(<UserListsPage />);
    
    const userListsPage = screen.getByTestId('UserListsPage');

    expect(userListsPage).toBeInTheDocument();
  });
});
import React, { FC } from 'react';
import HeaderLeftSide from '../HeaderLeftSide/HeaderLeftSide';
import MainMenu from '../MainMenu/MainMenu';
import styles from './Header.module.scss';

interface HeaderProps {}

const Header: FC<HeaderProps> = (props) => (
  <div className={styles.headerContainer} data-testid="Header">
    <div className={styles.headerContent}>
      <HeaderLeftSide />
      <MainMenu />
    </div>
  </div>
);

export default Header;

import React, { FC } from 'react';
import TileItem, { TileItemData } from '../TileItem/TileItem';
import styles from './Tile.module.scss';

interface TileProps {}

const Tile: FC<TileProps> = () => {
  let array = [
    {
      type: "product",
      data: {
        id: 1,
        title: "Товар 123",
        price: 1500,
        forecastedDeliveryDate: 1673890838
      }
    },
    {
      type: "product",
      data: {
        id: 2,
        title: "Новогодняя гирлянда, 30 шт.",
        price: 499,
        imageUrl: 'https://d.radikal.host/2023/01/07/product1.jpg',
        forecastedDeliveryDate: 1673559638
      }
    },
    {
      type: "product",
      data: {
        id: 3,
        title: "Ботинки зимние мужские Random Brand",
        price: 12890,
        imageUrl: 'https://b.radikal.host/2023/01/07/product2.jpg',
        forecastedDeliveryDate: 1679348438
      }
    },
    {
      type: "product",
      data: {
        id: 4,
        title: "Вода питьевая, 0.5 л",
        price: 12890,
        imageUrl: 'https://d.radikal.host/2023/01/07/product3.png',
        forecastedDeliveryDate: 1704231638
      }
    },
    {
      type: "product",
      data: {
        id: 5,
        title: "Товар 456",
        price: 457,
        forecastedDeliveryDate: 1673473238
      }
    },
    {
      type: "product",
      data: {
        id: 6,
        title: "Товар 789",
        price: 39999,
        forecastedDeliveryDate: 1673718038
      }
    },
    {
      type: "product",
      data: {
        id: 7,
        title: "Товар 012",
        price: 20,
        forecastedDeliveryDate: 1673732438
      }
    },
  ];

  return (
    <div className={styles.tileContainer} data-testid="Tile">
      {array.map(tileItem => {
        return (
          <TileItem key={tileItem.data.id}
                    type={tileItem.type}
                    data={tileItem.data as TileItemData}
          />
        )
      })}
    </div>
  );
};

export default Tile;

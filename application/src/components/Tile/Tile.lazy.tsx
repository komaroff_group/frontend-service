import React, { lazy, Suspense } from 'react';

const LazyTile = lazy(() => import('./Tile'));

const Tile = (props: JSX.IntrinsicAttributes & { children?: React.ReactNode; }) => (
  <Suspense fallback={null}>
    <LazyTile {...props} />
  </Suspense>
);

export default Tile;

import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import SearchHints from './SearchHints';

describe('<SearchHints />', () => {
  test('it should mount', () => {
    render(<SearchHints visible={true} items={[]} />);
    
    const searchHints = screen.getByTestId('SearchHints');

    expect(searchHints).toBeInTheDocument();
  });
});
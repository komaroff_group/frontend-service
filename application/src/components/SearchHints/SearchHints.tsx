import React, { FC } from 'react';
import styles from './SearchHints.module.scss';
import { Link } from "react-router-dom";

interface SearchHintsProps {
  visible: boolean,
  items: any[]
}

const SearchHints: FC<SearchHintsProps> = (props) => {

  const noHintsStub = (
    <div className={styles.noHintsStub}>
      Подсказок нет.
    </div>
  );

  const isNoHints = props.items.length === 0;

  return (
    <div className={styles.searchHints}
      style={{
        display: props.visible ? '' : 'none'
      }}
    >
      {
        isNoHints ?
          noHintsStub :
          props.items.map(hint => {
            return (
              <div key={hint.title} className={styles.searchHint}>
                <Link to={{
                  pathname: `/search`,
                  search: `?q=${hint.title}`
                }}
                >
                  {hint.title}
                </Link>
              </div>
            );
          })
      }
    </div>
  );
};

export default SearchHints;

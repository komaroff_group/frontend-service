import React, { FC } from 'react';
import styles from './Footer.module.scss';
import {Link} from 'react-router-dom';

interface FooterProps { }

const Footer: FC<FooterProps> = () => (
  <div className={styles.footerContainer} data-testid="Footer">
    <div className={styles.copyright}>
      <span>&copy; Sergey Komarov, 2023</span>

      <span>Разработано в образовательных целях</span>
    </div>
    {/* Links */}
    <ul className={styles.links}>
      <li>
        <Link className={styles.lightLink} 
              to={"about"}>
          О проекте TheMarketplace
        </Link>
      </li>
      <li>
        <a className={styles.lightLink} href="/">
          Вакансии
        </a>
      </li>
      <li>
        <a className={styles.lightLink} href="/">
          Стать продавцом
        </a>
      </li>
      <li>
        <a className={styles.lightLink} href="/">
          Открыть ПВЗ
        </a>
      </li>
      <li>
        <a className={styles.lightLink} href="/">
          Оплата
        </a>
      </li>
      <li>
        <a className={styles.lightLink} href="/">
          Доставка
        </a>
      </li>
      <li>
        <a className={styles.lightLink} href="/">
          Возврат и обмен
        </a>
      </li>
      <li>
        <a className={styles.lightLink} href="/">
          Пользовательское соглашение
        </a>
      </li>
    </ul>
    {/* Contacts */}
    <div className={styles.contacts}>
      <a href="https://t.me/Jay_Lim" rel='noreferrer' target="_blank"  className={styles.telegramIcon} title="Telegram"></a>
      <a href="https://vk.com/sergeykomarov98" rel='noreferrer' target="_blank" className={styles.vkIcon} title="VK"></a>
      <a href="shockshadow2013" rel='noreferrer' target="_blank" className={styles.skypeIcon} title="Skype"></a>
      <a href="mailto:shockshadow@yandex.ru" rel='noreferrer' target="_blank"  className={styles.emailIcon} title="E-mail"></a>
      <a href="https://gitlab.com/komaroff_group" rel='noreferrer' target="_blank"  className={styles.gitLabIcon} title="This project sources on the GitLab"></a>
    </div>
  </div>
);

export default Footer;

import React, { FC } from 'react';
import styles from './Logo.module.scss';
import { useNavigate } from 'react-router-dom';

interface LogoProps { 
}

const Logo: FC<LogoProps> = (props) => {
  const navigate = useNavigate();

  const onLogoClick = () => {
    navigate("/");
  }
  
  return (
    <div onClick={onLogoClick} className={styles.logo} data-testid="Logo">
      <div className={styles.titleContainer}>
        <div className={styles.title}>m</div>
      </div>
    </div>
  );
};

export default Logo;

import React from 'react';
import Main from '../Main/Main';
import styles from '../../App.module.scss';

function AboutProjectPage() {
  return (
    <div className={styles.root}>
      <Main>
        <div>
          <h1>
            О проекте TheMarketplace
          </h1>
          <div>
            Информация о проекте.
          </div>
        </div>
      </Main>
    </div>
  );
}

export default AboutProjectPage;

import React, { lazy, Suspense } from 'react';

const LazyAboutProjectPage = lazy(() => import('./AboutProjectPage'));

const AboutProjectPage = (props: JSX.IntrinsicAttributes & { children?: React.ReactNode; }) => (
  <Suspense fallback={null}>
    <LazyAboutProjectPage {...props} />
  </Suspense>
);

export default AboutProjectPage;

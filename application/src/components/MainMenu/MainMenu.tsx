import React, { FC } from 'react';
import styles from './MainMenu.module.scss';
import { useNavigate } from 'react-router-dom';

interface MainMenuProps {}

const MainMenu: FC<MainMenuProps> = () => {

  const navigate = useNavigate();

  const onMenuItemClick = (route: string) => {
    navigate(`/user/${route}`);
  };
  
  return (
    <ul className={styles.mainMenuContainer} data-testid="MainMenu">
      <li onClick={onMenuItemClick.bind(null, 'profile')}>
        <div className={styles.userIcon}></div>
        Профиль
      </li>
      <li onClick={onMenuItemClick.bind(null, 'orders')}>
        <div className={styles.ordersIcon}></div>
        Заказы
      </li>
      <li onClick={onMenuItemClick.bind(null, 'favorites')}>
        <div className={styles.favoritesIcon}></div>
        Избранное
      </li>
      <li onClick={onMenuItemClick.bind(null, 'waitings')}>
        <div className={styles.waitingListIcon}></div>
        Ожидание
      </li>
      <li onClick={onMenuItemClick.bind(null, 'cart')}>
        <div className={styles.cartIcon}></div>
        Корзина
      </li>
    </ul>
  )
};

export default MainMenu;

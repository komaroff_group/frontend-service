import React, { lazy, Suspense } from 'react';

const LazyUserOrdersPage = lazy(() => import('./UserOrdersPage'));

const UserOrdersPage = (props: JSX.IntrinsicAttributes & { children?: React.ReactNode; }) => (
  <Suspense fallback={null}>
    <LazyUserOrdersPage {...props} />
  </Suspense>
);

export default UserOrdersPage;

import React, { FC } from 'react';
import styles from './UserOrdersPage.module.scss';
import Main from '../Main/Main';

interface UserOrdersPageProps { }

const UserOrdersPage: FC<UserOrdersPageProps> = () => (
  <div className={styles.root}>
    <Main>
      <div>
        <h1>
          Заказы
        </h1>
        <div>
          Здесь будут Ваши заказы.
        </div>
      </div>
    </Main>
  </div>
);

export default UserOrdersPage;

import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import UserOrdersPage from './UserOrdersPage';

describe('<UserOrdersPage />', () => {
  test('it should mount', () => {
    render(<UserOrdersPage />);
    
    const userOrdersPage = screen.getByTestId('UserOrdersPage');

    expect(userOrdersPage).toBeInTheDocument();
  });
});
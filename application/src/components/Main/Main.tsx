import React, { FC } from 'react';
import styles from './Main.module.scss';
interface MainProps {
  children?: any
}

const Main: FC<MainProps> = (props) => (
  <div className={styles.mainContainer} data-testid="Main">
    {props.children}
  </div>
);

export default Main;

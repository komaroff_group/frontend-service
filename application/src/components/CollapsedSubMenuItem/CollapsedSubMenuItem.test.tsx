import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import CollapsedSubMenuItem, { HierarchicalCategory } from './CollapsedSubMenuItem';

describe('<CollapsedSubMenuItem />', () => {
  test('it should mount', () => {
    render(<CollapsedSubMenuItem categories={{} as HierarchicalCategory} />);

    const collapsedSubMenuItem = screen.getByTestId('CollapsedSubMenuItem');

    expect(collapsedSubMenuItem).toBeInTheDocument();
  });
});
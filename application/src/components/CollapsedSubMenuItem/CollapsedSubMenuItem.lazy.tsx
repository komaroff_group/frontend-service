import React, { lazy, Suspense } from 'react';
import { CollapsedSubMenuItemProps } from './CollapsedSubMenuItem';

const LazyCollapsedSubMenuItem = lazy(() => import('./CollapsedSubMenuItem'));

const CollapsedSubMenuItem = (props: JSX.IntrinsicAttributes & { children?: React.ReactNode; } & CollapsedSubMenuItemProps) => (
  <Suspense fallback={null}>
    <LazyCollapsedSubMenuItem {...props} />
  </Suspense>
);

export default CollapsedSubMenuItem;

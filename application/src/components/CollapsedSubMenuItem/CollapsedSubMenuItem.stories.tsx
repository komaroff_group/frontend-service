/* eslint-disable */
import CollapsedSubMenuItem, { HierarchicalCategory } from './CollapsedSubMenuItem';

export default {
  title: "CollapsedSubMenuItem",
};

export const Default = () => <CollapsedSubMenuItem categories={{} as HierarchicalCategory} />;

Default.story = {
  name: 'default',
};

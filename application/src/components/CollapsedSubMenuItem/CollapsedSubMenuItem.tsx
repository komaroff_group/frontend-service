import React, { FC } from 'react';
import { Link } from 'react-router-dom';
import styles from './CollapsedSubMenuItem.module.scss';

export interface Category {
  id: any,
  name: string
}

export interface HierarchicalCategory extends Category {
  subCategories: Category[]
}

export interface CollapsedSubMenuItemProps {
  categories: HierarchicalCategory
}

const CollapsedSubMenuItem: FC<CollapsedSubMenuItemProps> = ({categories: hierarchicalCategory}) => {

  return (
    <div className={styles.CollapsedSubMenuItem} data-testid="CollapsedSubMenuItem">
      <h4>{hierarchicalCategory.name}</h4>
      <ul className={styles.categoriesList}>
        {hierarchicalCategory.subCategories.map((category: Category) => {
          return (
            <Link className={styles.darkLink}
                  to={`/catalog/category?id=${category.id}`}
            >
              {category.name}
            </Link>
          );
        })}
      </ul>
    </div>
  );
};

export default CollapsedSubMenuItem;

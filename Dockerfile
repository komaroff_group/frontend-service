FROM node:alpine

WORKDIR /app

COPY . .

RUN apk update && apk add bash

WORKDIR /app/application

RUN npm install

RUN chmod +x /app/entrypoint.sh

ENTRYPOINT [ "/app/entrypoint.sh" ]